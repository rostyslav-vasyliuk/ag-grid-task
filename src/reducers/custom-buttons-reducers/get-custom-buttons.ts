import { GET_CUSTOM_BUTTONS, GET_CUSTOM_BUTTONS_SUCCESS } from '../../actions/actionKeys';

const initialState = {
  customButtons: [] as any,
};

const getCustomButtonReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_CUSTOM_BUTTONS:
      return {
        ...state,
      };
    case GET_CUSTOM_BUTTONS_SUCCESS:
      return {
        customButtons: action.payload,
      };
    default:
      return state;
  }
};

export default getCustomButtonReducer;
