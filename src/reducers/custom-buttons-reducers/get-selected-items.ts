import { GET_SELECTED_ITEMS } from '../../actions/actionKeys';

const initialState = {
  getSelectedItems: [] as any,
};

const getSelectedItemsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_SELECTED_ITEMS:
      return {
        getSelectedItems: action.payload,
      };
    default:
      return state;
  }
};

export default getSelectedItemsReducer;
