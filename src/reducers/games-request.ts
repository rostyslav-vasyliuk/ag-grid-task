import { FETCH_GAMES, FETCH_GAMES_SUCCESS } from '../actions/actionKeys';

interface GameInterface {
  title: string;
  year: string;
  rating: string;
  genre: string;
}

const initialStateMaxValue = {
  gamesItems: [] as GameInterface[],
};

const gamesReducer = (state = initialStateMaxValue, action: any) => {
  switch (action.type) {
    case FETCH_GAMES:
      return {
        ...state,
      };
    case FETCH_GAMES_SUCCESS:
      return {
        gamesItems: action.payload,
      };
    default:
      return state;
  }
};

export default gamesReducer;
