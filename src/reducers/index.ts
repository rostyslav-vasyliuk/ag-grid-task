import { combineReducers } from 'redux';
import getCustomButtonReducer from './custom-buttons-reducers/get-custom-buttons';
import getSelectedItemsReducer from './custom-buttons-reducers/get-selected-items';
import gameReducer from './games-request';
import saveGridApi from './save-grid-api';
import updateGameReducer from './update-game';

const rootReducer = combineReducers({
  saveGridApi,
  gameReducer,
  updateGameReducer,
  getCustomButtonReducer,
  getSelectedItemsReducer,
});

export default rootReducer;
