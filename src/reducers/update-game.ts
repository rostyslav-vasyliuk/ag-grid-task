import { UPDATE_GAME_REQUEST } from '../actions/actionKeys';

interface GameInterface {
  title: string;
  year: string;
  rating: string;
  genre: string;
}

const initialStateMaxValue = {
  games: [] as GameInterface[],
};

const updateGameReducer = (state = initialStateMaxValue, action: any) => {
  switch (action.type) {
    case UPDATE_GAME_REQUEST:
      return {
        games: action.payload,
      };
    default:
      return state;
  }
};

export default updateGameReducer;
