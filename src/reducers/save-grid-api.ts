import { SAVE_GRID_API } from '../actions/actionKeys';

const initialStateMaxValue = {
  gridApi: {} as any,
};

const saveGridApiReducer = (state = initialStateMaxValue, action: any) => {
  switch (action.type) {
    case SAVE_GRID_API:
      return {
        gridApi: action.payload,
      };
    default:
      return state;
  }
};

export default saveGridApiReducer;
