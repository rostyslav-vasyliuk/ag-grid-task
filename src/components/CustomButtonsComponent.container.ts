import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCustomButtons, sendGetRequest, sendPostRequest } from '../actions/actions';
import CustomButtonsComponent from './CustomButtonsComponent';

const mapStateToProps = (data: any) => ({
  customButtons: data.getCustomButtonReducer.customButtons,
  selectedItems: data.getSelectedItemsReducer.getSelectedItems,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getCustomButtons,
      sendPostRequest,
      sendGetRequest,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomButtonsComponent);
