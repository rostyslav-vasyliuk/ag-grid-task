import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './css/dockergrid.module.scss';
import { FormProps } from './interfaces/MongoGridInterfaces';

const AddGameForm = (props: FormProps) => (
  <div>
    <div className={styles.buttonsGroup}>
      <TextField
        label="Search"
        name="search"
        onChange={props.onSearch}
        className={styles.searchField}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={props.onSave}
        className={styles.deleteButton}
      >
        Save configuration
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={props.onDefault}
        className={styles.deleteButton}
      >
        To default
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={props.onDelete}
        className={styles.deleteButton}
      >
        Delete selected items
      </Button>
    </div>
    <div className={styles.formContainer}>
      <TextField
        label="Title"
        name="title"
        value={props.title}
        onChange={props.onFieldChange}
        className={styles.inputField}
      />
      <TextField
        label="Year"
        name="year"
        value={props.year}
        onChange={props.onFieldChange}
        className={styles.inputField}
      />
      <TextField
        label="Rating"
        name="rating"
        value={props.rating}
        onChange={props.onFieldChange}
        className={styles.inputField}
      />
      <TextField
        label="Genre"
        name="genre"
        value={props.genre}
        onChange={props.onFieldChange}
        className={styles.inputField}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={props.onSubmit}
        className={styles.submitButton}
        type="submit"
      >
        Add item
      </Button>
    </div>
    <Link to="/export-grid">
      <Button variant="contained" color="primary" className={styles.exportButton} type="submit">
        Export Grid
      </Button>
    </Link>
  </div>
);

export default AddGameForm;
