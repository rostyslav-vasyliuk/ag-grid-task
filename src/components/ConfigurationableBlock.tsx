import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import React from 'react';
import Swal from 'sweetalert2';
import styles from './css/configurationableBlock.module.scss';
import { Props, State } from './interfaces/ConfigurationableBlockInterface';

class ConfigurationableBlock extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      configurationCheckboxValue: false,
      valueSelectRequestType: 0,
      link: '',
    };
  }

  public onConfiguratableCheckbox = () => {
    this.setState({ configurationCheckboxValue: !this.state.configurationCheckboxValue });
  };

  public handleChangeSelect = () => {
    if (this.state.valueSelectRequestType === 0) {
      this.setState({ valueSelectRequestType: 1 });
    } else {
      this.setState({ valueSelectRequestType: 0 });
    }
  };

  public generateURL = () => {
    const selectedRows = this.props.gridApi.getSelectedRows();
    let url = 'http://localhost:8082/api/custom-button/';
    selectedRows.map((elem: any, i: number) => {
      if (i === 0) {
        url += `id[]=${elem._id}`;
      } else {
        url += `&id[]=${elem._id}`;
      }
      return url;
    });
    return url;
  };

  public onURLPreview = () => {
    Swal.fire('URL Preview', `${this.generateURL()}`, 'info');
    this.setState({ link: this.generateURL() });
  };

  public onSubmit = () => {
    if (!this.props.gridApi.getSelectedRows().length) {
      Swal.fire('Error', 'You should chose some items in the grid!', 'error');
      return;
    }
    if (this.state.valueSelectRequestType === 1) {
      const selectedRows = this.props.gridApi.getSelectedRows();
      this.props.sendPostRequest(selectedRows);
    }
    if (this.state.valueSelectRequestType === 0) {
      window.open(this.generateURL(), this.state.configurationCheckboxValue ? '_blank' : '_self');
    }
  };

  public render() {
    return (
      <div className={styles.container}>
        <div className={styles.configurationableButton}>
          <div className={styles.centering}>
            New tab:
            <Checkbox
              checked={this.state.configurationCheckboxValue ? true : false}
              onChange={this.onConfiguratableCheckbox}
              disabled={this.state.valueSelectRequestType ? true : false}
            />
          </div>
          <div className={styles.centering}>
            Request type:
            <Select
              value={this.state.valueSelectRequestType}
              onChange={this.handleChangeSelect}
              className={styles.selectStyle}
            >
              <MenuItem value={0}>GET</MenuItem>
              <MenuItem value={1}>POST</MenuItem>
            </Select>
          </div>
          <Button
            variant="contained"
            color="primary"
            disabled={this.state.valueSelectRequestType ? true : false}
            onClick={this.onURLPreview}
          >
            URL Preview
          </Button>
          <Button variant="contained" color="primary" onClick={this.onSubmit}>
            {this.state.valueSelectRequestType ? 'Submit POST' : 'Submit GET'}
          </Button>
        </div>
      </div>
    );
  }
}

export default ConfigurationableBlock;
