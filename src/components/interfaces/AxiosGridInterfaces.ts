interface ColumnsArgumentsInterface {
  headerName: string;
  field: string;
  width: number;
  cellRenderer?: string;
  rowDrag?: boolean;
}

interface RowsArgumentsInterface {
  athlete: string | null;
  age: number | null;
  country: string | null;
  year: string | null;
  date: string | null;
  sport: string | null;
  gold: number | null;
  silver: number | null;
  bronze: number | null;
  total: number;
}

interface FetchedData {
  items: RowsArgumentsInterface[];
  isLoading: boolean;
}

export interface MyProps {
  fetchItems: any;
  maxValue: number;
  saveMaxValue: any;
  saveGridApi: any;
  data: FetchedData;
}
export interface MyState {
  columnDefs: ColumnsArgumentsInterface[];
  rowData: RowsArgumentsInterface[];
  frameworkComponents: object;
  loadingOverlayComponent: string;
}
