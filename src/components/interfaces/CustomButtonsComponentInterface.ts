interface CustomButton {
  label: string;
  link: string;
}

export interface Props {
  customButtons: CustomButton[];
  gridApi: any;
  sendPostRequest: any;
}

export interface State {
  checkboxArray: boolean[];
  configurationCheckboxValue: boolean;
  valueSelectRequestType: number;
  link: string;
}
