interface CustomButton {
  label: string;
  link: string;
}

export interface Props {
  customButtons: CustomButton[];
  gridApi: any;
  sendPostRequest: any;
}

export interface State {
  configurationCheckboxValue: boolean;
  valueSelectRequestType: number;
  link: string;
}
