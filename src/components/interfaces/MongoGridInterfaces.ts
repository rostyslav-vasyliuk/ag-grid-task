export interface Props {
  fetchGames: any;
  gameData: any;
  postGame: any;
  deleteGame: any;
  updateGame: any;
  refreshedRow: any;
  saveGridApi: any;
  getCustomButtons: any;
  getSelectedItems: any;
  onSelectionChanged?: any;
}

export interface State {
  columnDefs: any;
  rowData: any;
  title: string;
  year: string;
  rating: string;
  genre: string;
  frameworkComponents: any;
}

export interface GridProps {
  columnDefs: any;
  rowData: any;
  onGridReady: any;
  onCellValueChanged: any;
  frameworkComponents?: any;
  onSelectionChanged?: any;
}

export interface FormProps {
  onDelete: any;
  title: any;
  year: any;
  genre: any;
  rating: any;
  onSubmit: any;
  onFieldChange: any;
  onSearch: any;
  onSave: any;
  onDefault: any;
}
