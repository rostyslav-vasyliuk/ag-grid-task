import { connect } from 'react-redux';
import ExportGrid from './ExportGrid';

const mapStateToProps = (data: any) => ({
  gridApi: data.saveGridApi.gridApi,
});

export default connect(
  mapStateToProps,
  null
)(ExportGrid);
