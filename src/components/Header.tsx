import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './css/header.module.scss';

const Header: React.FC = () => {
  return (
    <div className={styles.headerContainer}>
      <div className={styles.headerTitle}>AG GRID EXAMPLE</div>
      <div className={styles.navigationButtons}>
        <Link to="/" className={styles.link}>
          <div className={styles.navButton}>Grid</div>
        </Link>
        <Link to="/export-grid" className={styles.link}>
          <div className={styles.navButton}>Export Grid</div>
        </Link>
      </div>
    </div>
  );
};

export default Header;
