import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sendPostRequest } from '../actions/actions';
import ConfigurationableBlock from './ConfigurationableBlock';

const mapStateToProps = (data: any) => ({
  customButtons: data.getCustomButtonReducer.customButtons,
  gridApi: data.saveGridApi.gridApi,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      sendPostRequest,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigurationableBlock);
