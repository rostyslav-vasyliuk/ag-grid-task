import React from 'react';
import Swal from 'sweetalert2';
import AddGameForm from './AddGameForm';
import styles from './css/dockergrid.module.scss';
import GenreHeader from './headers/GenreHeader';
import RatingHeader from './headers/RatingHeader';
import TitleHeader from './headers/TitleHeader';
import YearHeader from './headers/YearHeader';
import { Props, State } from './interfaces/MongoGridInterfaces';
import MongoGrid from './MongoGrid';

class GridContainer extends React.Component<Props, State> {
  private agGridApis: any = {};
  constructor(props: any) {
    super(props);
    this.state = {
      columnDefs: [
        {
          headerName: 'Title',
          field: 'title',
          width: 250,
          checkboxSelection: true,
          headerComponent: 'titleHeaderComponent',
          filter: 'agTextColumnFilter',
        },
        {
          headerName: 'Year',
          field: 'year',
          filter: 'agTextColumnFilter',
          width: 250,
          headerComponent: 'yearHeaderComponent',
        },
        {
          headerName: 'Rating',
          field: 'rating',
          width: 250,
          headerComponent: 'ratingHeaderComponent',
          comparator: this.ratingComparator,
        },
        {
          headerName: 'Genre',
          field: 'genre',
          width: 250,
          headerComponent: 'genreHeaderComponent',
        },
      ],
      rowData: null,
      frameworkComponents: {
        titleHeaderComponent: TitleHeader,
        yearHeaderComponent: YearHeader,
        ratingHeaderComponent: RatingHeader,
        genreHeaderComponent: GenreHeader,
      },
      title: '',
      year: '',
      rating: '',
      genre: '',
    };
  }

  public async componentDidMount() {
    this.props.fetchGames();
    this.props.getCustomButtons();
  }

  public componentDidUpdate(prevProps: any, prevState: any) {
    if (this.props.gameData !== prevProps.gameData) {
      this.setState({ rowData: this.props.gameData });
    }
  }

  public onFieldChange = ({ target }: any) => {
    const name: any = target.name;
    // @ts-ignore
    this.setState({ [name]: target.value });
  };

  public onSubmit = () => {
    const { title, year, rating, genre } = this.state;
    if (!(title && year && rating && genre)) {
      Swal.fire('Warning!', 'You should fill all fields!', 'warning');
      return;
    }
    this.props.postGame({ title, year, rating, genre });
    this.setState({ rowData: this.props.gameData, year: '', title: '', rating: '', genre: '' });
  };

  public onDelete = async () => {
    const selectedRows = this.agGridApis.gridApi.getSelectedRows();
    this.props.deleteGame(selectedRows);
  };

  public onGridReady = (params: any) => {
    this.agGridApis.gridApi = params.api;
    this.props.saveGridApi(params.api);
    const sortModel = JSON.parse(String(localStorage.getItem('sortModel')));
    const filterModel = JSON.parse(String(localStorage.getItem('filterModel')));
    if (sortModel) {
      this.agGridApis.gridApi.setSortModel(sortModel);
    }
    if (filterModel) {
      this.agGridApis.gridApi.setFilterModel(filterModel);
    }
  };

  public onCellValueChanged = (params: any) => {
    const editedRow = params.data;
    this.props.updateGame(editedRow);
    params.node.setData(this.props.refreshedRow);
  };

  public onSave = () => {
    const currentSort = this.agGridApis.gridApi.getSortModel();
    const currentFilter = this.agGridApis.gridApi.getFilterModel();
    localStorage.setItem('sortModel', JSON.stringify(currentSort));
    localStorage.setItem('filterModel', JSON.stringify(currentFilter));
    Swal.fire('Success!', 'Configuration was saved!', 'success');
  };

  public onDefault = () => {
    this.agGridApis.gridApi.setFilterModel(null);
    this.agGridApis.gridApi.setSortModel(null);
    localStorage.clear();
  };

  public onSearch = (e: any) => {
    const instance = this.agGridApis.gridApi.getFilterInstance('title');
    instance.setModel({
      type: 'startsWith',
      filter: e.target.value,
    });
    this.agGridApis.gridApi.onFilterChanged();
  };

  public ratingComparator = (item1: any, item2: any) => parseFloat(item1) - parseFloat(item2);

  public getSelectedItemsFunction = () => this.agGridApis.gridApi.getSelectedRows();

  public onSelectionChanged = () => this.props.getSelectedItems(this.getSelectedItemsFunction());

  public render() {
    return (
      <div className={styles.container}>
        <MongoGrid
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}
          onGridReady={this.onGridReady}
          onCellValueChanged={this.onCellValueChanged}
          onSelectionChanged={this.onSelectionChanged}
          frameworkComponents={this.state.frameworkComponents}
        />
        <AddGameForm
          onDelete={this.onDelete}
          title={this.state.title}
          year={this.state.year}
          genre={this.state.genre}
          rating={this.state.rating}
          onSubmit={this.onSubmit}
          onFieldChange={this.onFieldChange}
          onSearch={this.onSearch}
          onSave={this.onSave}
          onDefault={this.onDefault}
        />
      </div>
    );
  }
}

export default GridContainer;
