import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import styles from './css/exportgrid.module.scss';

const ExportGrid = (props: any) => {
  const [filename, setFilename] = useState('');

  const onExport = () => {
    const params: any = {
      fileName: filename,
    };

    props.gridApi.exportDataAsCsv(params);
  };

  const onInputChange = (e: any) => {
    setFilename(e.target.value);
  };

  return (
    <div className={styles.container}>
      <p className={styles.title}>Export your grid to CSV</p>
      <div className={styles.userFields}>
        <TextField label="Name of file" className={styles.inputStyle} onChange={onInputChange} />
        <Button
          disabled={props.gridApi ? false : true}
          value="Convert"
          variant="contained"
          color="primary"
          className={styles.buttonStyle}
          onClick={onExport}
        >
          Convert
        </Button>
      </div>
    </div>
  );
};

export default ExportGrid;
