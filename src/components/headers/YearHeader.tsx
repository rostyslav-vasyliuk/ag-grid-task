import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styles from '../css/gridHeaders.module.scss';
import { HeaderProps, HeaderState } from './headerInterfaces';

class YearHeader extends React.Component<HeaderProps, HeaderState> {
  constructor(props: any) {
    super(props);
    this.state = {
      ascSort: false,
      descSort: false,
    };
  }

  public onDesc = () => {
    if (!this.state.descSort) {
      this.props.setSort('desc');
    }
    if (this.state.descSort) {
      this.props.api.setSortModel(null);
    }

    this.setState({ descSort: !this.state.descSort, ascSort: false });
  };

  public onAsc = () => {
    if (!this.state.ascSort) {
      this.props.setSort('asc');
    } else {
      this.props.api.setSortModel(null);
    }

    this.setState({ ascSort: !this.state.ascSort, descSort: false });
  };

  public render() {
    const { ascSort, descSort } = this.state;
    return (
      <div className={styles.headerContainer}>
        <div>
          <FontAwesomeIcon icon="clock" className={styles.headerIcon} />
          <span className={styles.titleText}>Year</span>
        </div>
        <div className={styles.arrowsContainer}>
          <div onClick={this.onDesc}>
            <FontAwesomeIcon
              icon="arrow-down"
              className={
                descSort ? [styles.arrowIcon, styles.activeIcon].join(' ') : styles.arrowIcon
              }
            />
          </div>
          <div onClick={this.onAsc}>
            <FontAwesomeIcon
              icon="arrow-up"
              className={
                ascSort ? [styles.arrowIcon, styles.activeIcon].join(' ') : styles.arrowIcon
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default YearHeader;
