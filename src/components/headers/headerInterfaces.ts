export interface HeaderState {
  ascSort: boolean;
  descSort: boolean;
}

export interface HeaderProps {
  setSort: any;
  api: any;
}
