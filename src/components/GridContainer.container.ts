import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  deleteGame,
  fetchGames,
  getCustomButtons,
  getSelectedItems,
  postGame,
  saveGridApi,
  updateGame,
} from '../actions/actions';
import GridContainer from './GridContainer';

const mapStateToProps = (data: any) => ({
  gameData: data.gameReducer.gamesItems,
  refreshedRow: data.updateGameReducer.games,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      fetchGames,
      postGame,
      deleteGame,
      updateGame,
      saveGridApi,
      getCustomButtons,
      getSelectedItems,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GridContainer);
