import React from 'react';

const NotFound: React.FC = () => {
  return (
    <div>
      <h1>We couldnt find page you have requested!</h1>
    </div>
  );
};

export default NotFound;
