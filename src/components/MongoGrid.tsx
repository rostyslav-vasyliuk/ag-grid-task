import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import { AgGridReact } from 'ag-grid-react';
import React from 'react';
import styles from './css/dockergrid.module.scss';
import { GridProps } from './interfaces/MongoGridInterfaces';

const MongoGrid = (props: GridProps) => (
  <div className={styles.container}>
    <div>
      <p className={styles.title}>AG-Grid with Mongo data</p>
    </div>
    <div className={['ag-theme-material', styles.mongoGridSize].join(' ')}>
      <AgGridReact
        columnDefs={props.columnDefs}
        rowData={props.rowData}
        onGridReady={props.onGridReady}
        animateRows={true}
        rowSelection="multiple"
        reactNext={true}
        onCellValueChanged={props.onCellValueChanged}
        frameworkComponents={props.frameworkComponents}
        onSelectionChanged={props.onSelectionChanged}
        defaultColDef={{
          sortable: true,
          resizable: true,
          editable: true,
        }}
      />
    </div>
  </div>
);

export default MongoGrid;
