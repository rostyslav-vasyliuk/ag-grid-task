import React from 'react';
import ConfigurationableBlock from './ConfigurationableBlock.container';
import CustomButtonsComponent from './CustomButtonsComponent.container';
import GridContainer from './GridContainer.container';

const MainContainer = () => (
  <>
    <CustomButtonsComponent />
    <ConfigurationableBlock />
    <GridContainer />
  </>
);

export default MainContainer;
