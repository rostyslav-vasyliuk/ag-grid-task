import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
import Swal from 'sweetalert2';
import styles from './css/customButton.module.scss';

class CustomButtonsComponent extends React.Component<any, any> {
  public componentDidMount() {
    this.props.getCustomButtons();
  }

  public generateURL = (fullURL = true) => {
    const selectedRows = this.props.selectedItems;
    let url: string = '';
    if (fullURL) {
      url = 'http://localhost:8082/api/custom-button/';
    } else {
      url = '/api/custom-button/';
    }
    selectedRows.map((elem: any, i: number) => {
      if (i === 0) {
        url += `id[]=${elem._id}`;
      } else {
        url += `&id[]=${elem._id}`;
      }
      return url;
    });
    return url;
  };

  public onCustomButtonSubmit = (buttonData: any) => {
    if (!this.props.selectedItems.length) {
      Swal.fire('Error', 'You should chose some items to make request!', 'error');
      return;
    }

    if (buttonData.requestType === 'POST') {
      this.props.sendPostRequest(this.props.selectedItems);
      return;
    }

    if (buttonData.requestType === 'GET' && buttonData.tab) {
      window.open(this.generateURL(), buttonData.tab === 'new' ? '_blank' : '_self');
      return;
    }

    if (buttonData.requestType === 'GET' && !buttonData.tab) {
      this.props.sendGetRequest(this.generateURL(false));
      return;
    }
  };

  public render() {
    return (
      <div className={styles.container}>
        {!this.props.customButtons.length ? (
          <div className={styles.configurationableButton}>
            <CircularProgress />
          </div>
        ) : (
          <div className={styles.configurationableButton}>
            {this.props.customButtons.map((button: any, i: number) => (
              <Button
                variant="contained"
                color="primary"
                key={button.label}
                // tslint:disable-next-line
                onClick={() => this.onCustomButtonSubmit(button)}
              >
                {button.label}
              </Button>
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default CustomButtonsComponent;
