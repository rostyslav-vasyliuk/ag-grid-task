import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowDown,
  faArrowUp,
  faBook,
  faClock,
  faHeading,
  faMedal,
  faStar,
} from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Routes from './Routes';

library.add(faMedal, faArrowDown, faArrowUp, faClock, faStar, faBook, faHeading);

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
};

export default App;
