import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ExportGrid from './components/ExportGrid';
import Header from './components/Header';
import MainContainer from './components/MainContainer';
import NotFound from './components/NotFound';

const Routes = () => (
  <React.Fragment>
    <Header />
    <Switch>
      <Route exact={true} path="/" component={MainContainer} />
      <Route exact={true} path="/export-grid" component={ExportGrid} />
      <Route component={NotFound} />
    </Switch>
  </React.Fragment>
);

export default Routes;
