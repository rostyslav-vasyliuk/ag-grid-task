import axiosInstance from '../http/custom-buttons-request';

export const getGames = () => axiosInstance.get('/api/games/get-top');

export const addGame = (body: any) => axiosInstance.post('/api/games/add-game', body);

export const deleteGames = (items: any) =>
  axiosInstance.delete('/api/games/delete-games', { data: { itemsToDelete: items } });

export const updateGameRequest = (body: any) => axiosInstance.post('/api/games/update-game', body);

export const getButtons = () => axiosInstance.get('/api/custom-button/get-buttons');

export const getGamesGet = (url: string) => axiosInstance.get(url);

export const getGamesPost = (array: any) =>
  axiosInstance.post('/api/custom-button/get-games-post', { gamesArray: array });
