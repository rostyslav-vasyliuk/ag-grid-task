import axios from 'axios';
import Swal from 'sweetalert2';

const instance = axios.create({
  baseURL: 'http://localhost:8082',
});

instance.interceptors.request.use(
  config => config,
  error => Swal.fire('Request error!', 'Request failed!', 'error')
);

instance.interceptors.response.use(
  response => response,
  error => {
    if (error.response) {
      if (error.response.status >= 400 && error.response.status <= 500) {
        Swal.fire('Error!', `Code: ${error.response.status} ${error.response.message}`, 'error');
      }
    } else {
      Swal.fire('Error!', `Error occurred!`, 'error');
    }
  }
);

export default instance;
