import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import Swal from 'sweetalert2';
import { GET_CUSTOM_BUTTONS, SEND_GET_REQUEST, SEND_POST_REQUEST } from '../actions/actionKeys';
import {
  getCustomButtonsSuccess,
  sendGetRequestSuccess,
  sendPostRequestSuccess,
} from '../actions/actions';
import { getButtons, getGamesGet, getGamesPost } from '../http/requests';

export const getCustomButtons = (action$: any) =>
  action$
    .ofType(GET_CUSTOM_BUTTONS)
    .switchMap(() => getButtons())
    .map((items: any) => items.data)
    .map((items: any) => getCustomButtonsSuccess(items))
    .catch(() => Swal.fire('Error', 'Error in fetcing data! Plese, try again later', 'error'));

export const sendPostRequest = (action$: any) =>
  action$
    .ofType(SEND_POST_REQUEST)
    .switchMap((obj: any) => getGamesPost(obj.payload))
    .map(() => sendPostRequestSuccess())
    .catch(() => Swal.fire('Error', 'Error in fetcing data! Plese, try again later', 'error'));

export const sendGetRequest = (action$: any) =>
  action$
    .ofType(SEND_GET_REQUEST)
    .switchMap((obj: any) => getGamesGet(obj.payload))
    .map(() => sendGetRequestSuccess())
    .catch(() => Swal.fire('Error', 'Error in fetcing data! Plese, try again later', 'error'));
