import { combineEpics } from 'redux-observable';
import { getCustomButtons, sendGetRequest, sendPostRequest } from './custom-button-epic';
import { deleteGames, getGamesItems, postGame, updateGame } from './game-epic';

const rootEpic = combineEpics(
  getGamesItems,
  postGame,
  deleteGames,
  updateGame,
  getCustomButtons,
  sendPostRequest,
  sendGetRequest
);

export default rootEpic;
