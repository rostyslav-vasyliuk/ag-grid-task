import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import Swal from 'sweetalert2';
import {
  DELETE_GAME_REQUEST,
  FETCH_GAMES,
  POST_GAME_REQUEST,
  UPDATE_GAME_REQUEST,
} from '../actions/actionKeys';
import { fetchGames, fetchGamesSuccess, refreshUpdatedGame } from '../actions/actions';
import { addGame, getGames, updateGameRequest } from '../http/requests';

export const getGamesItems = (action$: any) =>
  action$
    .ofType(FETCH_GAMES)
    .switchMap(() => getGames())
    .map((items: any) => items.data)
    .map((items: any) => fetchGamesSuccess(items));

export const postGame = (action$: any) =>
  action$
    .ofType(POST_GAME_REQUEST)
    .switchMap((obj: any) => addGame(obj.payload))
    .map((items: any) => {
      Swal.fire('Success!', 'Item was succesfully added!', 'success');
      return fetchGames();
    })
    .catch((err: any) => console.log(err));

export const deleteGames = (action$: any) =>
  action$
    .ofType(DELETE_GAME_REQUEST)
    .switchMap((obj: any) => deleteGames(obj.payload))
    .map((items: any) => {
      Swal.fire('Success!', 'Item was succesfully deleted!', 'success');
      return fetchGames();
    })
    .catch((err: any) => console.log(err));

export const updateGame = (action$: any) =>
  action$
    .ofType(UPDATE_GAME_REQUEST)
    .switchMap((obj: any) => updateGameRequest(obj.payload))
    .map((refreshedGame: any) => refreshUpdatedGame(refreshedGame))
    .catch((err: any) => console.log(err));
