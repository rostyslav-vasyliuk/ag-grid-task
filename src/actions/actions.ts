import {
  DELETE_GAME_REQUEST,
  FETCH_GAMES,
  FETCH_GAMES_SUCCESS,
  GET_CUSTOM_BUTTONS,
  GET_CUSTOM_BUTTONS_SUCCESS,
  GET_SELECTED_ITEMS,
  POST_GAME_REQUEST,
  REFRESH_UPDATED_GAME,
  SAVE_GRID_API,
  SEND_GET_REQUEST,
  SEND_GET_REQUEST_SUCCESS,
  SEND_POST_REQUEST,
  SEND_POST_REQUEST_SUCCESS,
  UPDATE_GAME_REQUEST,
} from './actionKeys';

export const getCustomButtons = () => ({
  type: GET_CUSTOM_BUTTONS,
});

export const getCustomButtonsSuccess = (items: any) => ({
  type: GET_CUSTOM_BUTTONS_SUCCESS,
  payload: items,
});

export const sendPostRequest = (items: any) => ({
  type: SEND_POST_REQUEST,
  payload: items,
});

export const sendPostRequestSuccess = () => ({
  type: SEND_POST_REQUEST_SUCCESS,
});

export const sendGetRequest = (url: any) => ({
  type: SEND_GET_REQUEST,
  payload: url,
});

export const sendGetRequestSuccess = () => ({
  type: SEND_GET_REQUEST_SUCCESS,
});

export const saveGridApi = (gridApi: any) => ({
  type: SAVE_GRID_API,
  payload: gridApi,
});

export const fetchGames = () => ({
  type: FETCH_GAMES,
});

export const fetchGamesSuccess = (items: any) => ({
  type: FETCH_GAMES_SUCCESS,
  payload: items,
});

export const postGame = (game: any) => ({
  type: POST_GAME_REQUEST,
  payload: game,
});

export const deleteGame = (games: any) => ({
  type: DELETE_GAME_REQUEST,

  payload: games,
});

export const updateGame = (game: any) => ({
  type: UPDATE_GAME_REQUEST,
  payload: game,
});

export const refreshUpdatedGame = (game: any) => ({
  type: REFRESH_UPDATED_GAME,
  payload: game,
});

export const getSelectedItems = (func: any) => ({
  type: GET_SELECTED_ITEMS,
  payload: func,
});
